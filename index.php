<html>
<head>
<title>Web Terminal</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script src="assets/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="funclib.js"></script>
<script type="text/javascript" src="terminal.js"></script>
<script type="text/javascript" src="session1.js"></script>
<script type="text/javascript" src="controller.js"></script>
</head>
<body onLoad="start()">
<audio id="key_click1">
  <source src="audio/key_click1.wav" type="audio/wav">
</audio>
<audio id="key_click2">
  <source src="audio/key_click2.wav" type="audio/wav">
</audio>
<audio id="key_click3">
  <source src="audio/key_click3.wav" type="audio/wav">
</audio>
<audio id="key_click4">
  <source src="audio/key_click4.wav" type="audio/wav">
</audio>
<audio id="key_click5">
  <source src="audio/key_click5.wav" type="audio/wav">
</audio>
<?php
echo '<div id="screen"></div>';
echo '<div id="content"></div>';
?>
</body>
</html>