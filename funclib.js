/* 
* Function Library
* By Ryan Bains-Jordan
*/
function session(ssName, ssVersion, ssAuthor) {
	this.sessionName = ssName;
	this.sessionVersion = ssVersion;
	this.sessionAuthor = ssAuthor;
}

// Function to write text to the screen
function writeToScreen(input, styleClass) {
	
	// Create Text Elements and fill the node
	var span = document.createElement("span");
	if (styleClass) {
		span.setAttribute("class", styleClass);
	}
	var br = document.createElement("br");
	var textInput = document.createTextNode(input);
	span.appendChild(textInput);
	
	// Write content to the screen
	var contentArea = document.getElementById("content");
	contentArea.appendChild(span);
	contentArea.appendChild(br);
}

// Function to create a line break element <br/>
function lineBreak() {
	var br = document.createElement("br");
	var contentArea = document.getElementById("content");
	contentArea.appendChild(br);
}
