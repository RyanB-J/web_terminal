/* 
 * Start Sessions File
 * By Ryan Bains-Jordan
 */

const sessionKey = "controller";

var sessionsArray = new Array();
sessionsArray[0] = new session(ss1_name, ss1_version, ss1_author);
sessionsArray[1] = new session("Session 2", "0.0.2", "Ryan Bains-Jordan");

function sessionInit() {
  $.getScript('terminal.js', function() {
    alert('Terminal Loaded');
  });
}

// Function to list all of the sessions in the sessionsArray
function listSessions() {
  writeToScreen("List of Current Sessions", "orange");
  lineBreak();
  var sessionCount = 0;
  for (var i = 0; i < sessionsArray.length; i++) {
    writeToScreen(sessionsArray[i].sessionName +
      " (" +
      sessionsArray[i].sessionVersion +
      ") " +
      sessionsArray[i].sessionAuthor, "list");
    sessionCount++;
  }
  lineBreak();
  writeToScreen("Total Found: " + sessionCount);
}

// Function that controlls the start of the program
function start() {
  sessionInit();
  writeToScreen("Web Terminal (" + terminalVersion + ")");
  writeToScreen("By Ryan Bains-Jordan");
  newCommand(sessionChoice);
}

function sessionChoice(argument) {

  var choice = argument.textContent;

  switch (choice) {
    case "load session 1":
      writeToScreen("Starting Session 1...");
      startSession1();
      break;
    case "load session 2":
      writeToScreen("Starting Session 2...");
      break;
    case "list sessions":
      listSessions();
      newCommand(sessionChoice);
      break;
    default:
      writeToScreen(choice + ": command not found.");
      newCommand(sessionChoice);
  }
}