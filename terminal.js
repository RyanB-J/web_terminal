/* 
 * Terminal.js
 * Local framework that handles the input box and events
 * By Ryan Bains-Jordan
 */
const terminalVersion = "0.0.1";
const commandSymbol = "$> ";

function newCommand(processFunction) {
	
	// Add the start of command symbol
	var span = document.createElement("span");
	span.setAttribute("class", "commandStart");
	var commandStart = document.createTextNode(commandSymbol);
	span.appendChild(commandStart);

	// Create the text field
	var textField = document.createElement("input");
	textField.setAttribute("type", "text");
	textField.setAttribute("maxlength", "100");
	
	// Append the new command line to the content area
	var contentArea = document.getElementById("content");
	contentArea.appendChild(span);
	contentArea.appendChild(textField);
	textField.focus();
	
	textField.addEventListener("keydown", function(e) {
		
		// Play a Keyboard click sound
		var randNum = Math.floor((Math.random() * 5) + 1);
		switch(randNum) {
			case 1: 
				document.getElementById("key_click1").play();
				break;
			case 2:
				document.getElementById("key_click2").play();
				break;
			case 3:
				document.getElementById("key_click3").play();
				break;
			case 4:
				document.getElementById("key_click4").play();
				break;
			case 5:
				document.getElementById("key_click5").play();
				break;
		}
			
		// Run the following scripts if RETURN is clicked
		if (e.keyCode == 13) {
			
			// Append textInput
			var textInput = document.createTextNode(textField.value);
			contentArea.appendChild(textInput);
			
			// Remove existing text field
			textField.parentNode.removeChild(textField);
			
			//Increment the line
			var lineBreak = document.createElement("br");
			contentArea.appendChild(lineBreak);	
			
			// Submit the input to the event function
			processFunction(textInput);
		}
	}, false);	
}