/* 
* Session 1
* By Ryan Bains-Jordan
*/

/*global writeToScreen*/
/*gloabal lineBreak*/

const ss1_name = "Session 1"
const ss1_version = "0.0.1";
const ss1_author = "Ryan Bains-Jordan";

function startSession1() {
	
	// Create the intro text nodes
	writeToScreen("Session 1 Version(" + ss1_version + ")");
	writeToScreen("By Ryan Bains-Jordan");
	writeToScreen("Type help for list of options");
	
	/*global setCommandLine*/
    setCommandLine();
    getSessionChoice();
    
}


function getSessionChoice() {
	
	var textField = document.getElementById("text_field");
	var contentArea = document.getElementById("content");
		
	// Listen for the enter key
	textField.addEventListener("keydown", function(e) {
		if (e.keyCode == 13) {

			// Append textInput
			var textInput = document.createTextNode(textField.value);
			contentArea.appendChild(textInput);
			
			// Remove existing text field
			textField.parentNode.removeChild(textField);
			
			//Increment the line
			var lineBreak = document.createElement("br");
			contentArea.appendChild(lineBreak);	
			
			// Submit the input to the event function
			processChoice1(textInput);
		}
	}, false);	
	
}


// These are Commands Relevent to Session 1
function processChoice1(argument) {
	
	var sessionChoice = argument.textContent;
	
	switch(sessionChoice) {
	    case "command 1":
	        writeToScreen("Doing Command 1...");
	        /*global startSession1*/
	        startSession1();
	        break;
	    case "command 2":
	        writeToScreen("Doing Command 2...");
	        break;
	    default:
	        writeToScreen("List of Session 1 commands: c1 or c2 ...");
	        setCommandLine();
    		getSessionChoice();
	}
}
